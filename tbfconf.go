package tbfconf

import (
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/floodcode/tbf"
)

// Start configures bot from environment variables and runs a bot
func Start(bot *tbf.TelegramBotFramework) error {
	if os.Getenv("TBF_WH_ENABLED") == "true" {
		whKeyFile, _ := ioutil.TempFile("", "private.*.key")
		whKeyFile.Write([]byte(os.Getenv("TBF_WH_KEY")))
		whKeyFile.Close()

		whCertFile, _ := ioutil.TempFile("", "public.*.pem")
		whCertFile.Write([]byte(os.Getenv("TBF_WH_CERT")))
		whCertFile.Close()

		whHost := os.Getenv("TBF_WH_HOST")
		whSlug := os.Getenv("TBF_WH_SLUG")
		whNoTLS := os.Getenv("TBF_WH_NO_TLS") == "true"

		whPort, _ := strconv.Atoi(os.Getenv("TBF_WH_PORT"))
		whNoTLSPort, _ := strconv.Atoi(os.Getenv("TBF_WH_NO_TLS_PORT"))

		return bot.Listen(tbf.ListenConfig{
			Host:         whHost,
			Port:         uint16(whPort),
			KeyFilename:  whKeyFile.Name(),
			CertFilename: whCertFile.Name(),
			Slug:         whSlug,
			NoTLS:        whNoTLS,
			NoTLSPort:    uint16(whNoTLSPort),
		})
	}

	pollDelay, _ := strconv.Atoi(os.Getenv("TBF_POLL_DELAY"))

	return bot.Poll(tbf.PollConfig{
		Delay: pollDelay,
	})
}
